# Données de test pour la BDD

Il  y a deux fichiers csv pour les données de test.
* - `demande.csv`
* - `individus.csv`
    
Dans ses fichiers, il y a quelques lignes dedans pour faire les tests.

Le fichier `imp_data_test.php` permet d'importer les données des deux fichiers dans la base.

Il faut garder la première ligne du fichier car c'est les noms de nos colonnes de table.

# Description fichier csv

## demande.csv

Les différentes colonnes :
* `num_ind` : un nombre entier int(15)
* `sexe` : une lettre varchar(1)
* `nom_usage` : plusieurs lettres varchar(250)
* `nom_usage_sans` : plusieurs lettres varchar(250)
* `nom_civil` : plusieurs lettres varchar(250)
* `prenom` : plusieurs lettres varchar(250)
* `prenom_sans` : plusieurs lettres varchar(250)
* `annee_naissance` : un nombre entier de 4 chiffre int(4)
* `mois_naissance` : un nombre entier int(2)
* `jour_naissance` : un nombre entier int(2)
__

## individus.csv

Les différentes colonnes :
* `num_ind` : un nombre entier int(15)
* `sexe` : une lettre varchar(1)
* `nom_usage` : plusieurs lettres varchar(250)
* `nom_usage_sans` : plusieurs lettres varchar(250)
* `nom_civil` : plusieurs lettres varchar(250)
* `prenom` : plusieurs lettres varchar(250)
* `prenomd` : plusieurs lettres varchar(250)
* `prenomt` : plusieurs lettres varchar(250)
* `annee_naissance` : un nombre entier de 4 chiffre int(4)
* `mois_naissance` : un nombre entier int(2)
* `jour_naissance` : un nombre entier int(2)
* `adresse` : plusieurs lettres varchar(250)
* `mdr` : plusieurs lettres varchar(250)
* `telephone` : plusieurs lettres varchar(250)
* `mail_mdr` : plusieurs lettres varchar(250)
* `libelle` : plusieurs lettres varchar(250)
* `code` : plusieurs lettres varchar(50)