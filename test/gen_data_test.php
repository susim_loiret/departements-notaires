<?php

// Génère un échantillon de données anonymisées à partir des tables contenant les données d'origine

include_once '../notaires_fonctions.php';

// Nom de la base contenant les vraies données à anonymiser, puis extraire dans fichier CSV
$base_source = "notaires_test";

function genDemandes() 
{
  global $connect, $base_source;
  
  $nomFic = "demande.csv";
  
  // Sélectionne les 1000 premiers noms pour faire une sélection au hasard après 
  $result = $connect->query ("select distinct(nom_usage) from $base_source.demande limit 1000");
  
  $noms = [];
  while ($val = $result->fetch()) {
    $noms[] = $val['nom_usage'];
  }  
  
  // Crée le fichier
  $file = fopen($nomFic, "w");
  // Ligne d'entête
  fwrite($file, '"num_ind";"sexe";"nom_usage";"nom_usage_sans";"nom_civil";"prenom";"prenom_sans";"annee_naissance";"mois_naissance";"jour_naissance"'. "\r\n");

  // Sélectionneles 250 premiers pour affecter un nom et une date de naissance au hasard
  $result = $connect->query ("select * from $base_source.demande limit 250");
  
  $i = 0;
  while ($val = $result->fetch()) {
    $i++;
    $nom = randomNom($noms);
    $line = sprintf('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"'. "\r\n",
        $i,
        $val["sexe"],
        $nom,
        $nom, //nom_usage
        "", // nom_civile
        $val["prenom"],
        $val["prenom_sans"],
        randomAnnee(),
        randomMois(), 
        randomJour() 
        );
    fwrite($file, $line);
  }  
  fclose($file);
}

function genIndividus()
{
  global $connect, $base_source;
  
  
  $nomFic = "individus.csv";
  
  // Sélectionne les 2000 premiers noms pour faire une sélection au hasard après
  $result = $connect->query ("select distinct(nom_usage) from $base_source.individus limit 2000");
  $noms = [];
  while ($val = $result->fetch()) {
    $noms[] = $val['nom_usage'];
  }
  // Sélectionne les MDR pour faire une sélection au hasard après
  $result = $connect->query ("select distinct(mdr) from $base_source.individus");
  $mdrs = [];
  while ($val = $result->fetch()) {
    $mdrs[] = $val['mdr'];
  }
  
  // Crée le fichier
  $file = fopen($nomFic, "w");
  // Ligne d'entête
  fwrite($file, '"num_ind";"sexe";"nom_usage";"nom_usage_sans";"nom_civil";"prenom";"prenomd";"prenomt";"annee_naissance";"mois_naissance";"jour_naissance";"adresse";"mdr";"telephone";"mail_mdr";"libelle";"code"'. "\r\n");
  
  // Sélectionneles 250 premiers pour affecter un nom et une date de naissance au hasard
  $result = $connect->query ("select * from notaires_test.individus limit 500");
  
  $i = 0;
  while ($val = $result->fetch()) {
    $i++;
    $nom = randomNom($noms);
    $mdr = randomNom($mdrs);
    $line = sprintf('"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s";"%s"'. "\r\n",
        $i,
        $val["sexe"],
        $nom,
        $nom, //nom_usage_sans
        "", // nom_civil
        $val["prenom"],
        $val["prenomd"],
        $val["prenomt"],
        randomAnnee(),
        randomMois(),
        randomJour(),
        $mdr, // adresse
        $mdr, // mdr identique adresse
        "04 69 69 69 69", // telephone
        "mdr@xxx.fr", // mail_mdr
        $val["libelle"],
        $val["code"]
        
        );
    fwrite($file, $line);
  }
  fclose($file);
}

function randomNom($noms) 
{
  $max = count($noms);
  return $noms[rand(0, $max-1)];
}

function randomAnnee() 
{
  return rand(1900, 1980);
}

function randomMois()
{
  return rand(1, 12);
}

function randomJour()
{
  return rand(1, 28);
}

genDemandes();
genIndividus();

?>