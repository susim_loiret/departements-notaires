﻿<?php 
require_once('notaires_fonctions.php');

sessionCheck();

if(isAdmin()){
	ob_end_clean();
	require_once ('vendor/jpgraph/jpgraph.php');
	require_once ('vendor/jpgraph/jpgraph_bar.php');
	require_once ('vendor/jpgraph/jpgraph_line.php');

	$mois = date('n');
	$annee = date('y');
	
	// Variable mensuelles en texte (Mois_Moins_Un, _Deux, _Trois, _Quatre)
	$mois_actu = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois+1, 0, $annee)));
	$mois_m_u = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois, 0, $annee)));
	$mois_m_d = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois-1, 0, $annee)));
	$mois_m_t = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0, $mois-2, 0, $annee)));
	$mois_m_q = utf8_encode(strftime("%B-%y ", mktime(0, 0, 0,$mois-3, 0, $annee)));
	
	//Variables mensuelles en chiffre (Mois_Moins_Un_Numérique, _Deux_Numérique, _Trois_Numérique, _Quatre_Numérique)
	$mactun = date("n");
	$annee = date('Y');
	if ($mactun == 1)
	{
		$mmun= 12; $amun=$annee-1;
		$mmdn= 11; $amdn=$annee-1;
		$mmtn = 10; $amtn=$annee-1;
		$mmqn = 9; $amqn=$annee-1;
	}
	elseif ($mactun == 2)
	{
		$mmun= 1; $amun=$annee;
		$mmdn= 12; $amdn=$annee-1;
		$mmtn = 11; $amtn=$annee-1;
		$mmqn = 10;	$amqn=$annee-1;
	}
	elseif ($mactun == 3)
	{
		$mmun= 2; $amun=$annee;
		$mmdn= 1; $amdn=$annee;
		$mmtn = 12; $amtn=$annee-1;
		$mmqn = 11;	$amqn=$annee-1;
	}
		elseif ($mactun == 4)
	{
		$mmun= 3; $amun=$annee;
		$mmdn= 2; $amdn=$annee;
		$mmtn = 1; $amtn=$annee;
		$mmqn = 12;	$amqn=$annee-1;
	}
	else
	{
		$mmun= date("n") -1; $amun=$annee;
		$mmdn= date("n")-2; $amdn=$annee;
		$mmtn = date("n")-3; $amtn=$annee;
		$mmqn = date("n")-4; $amqn=$annee;
	}

	//Recup
	$data1y=array(starepnat($mactun ,"1",$annee),starepnat($mmun ,"1",$amun),starepnat($mmdn,"1",$amdn),starepnat($mmtn,"1",$amtn),starepnat($mmqn,"1",$amqn));
	//Indus
	$data2y=array(starepnat($mactun,"2",$annee),starepnat($mmun ,"2",$amun),starepnat($mmdn,"2",$amdn),starepnat($mmtn,"2",$amtn),starepnat($mmqn,"2",$amqn));
	//Ambigus
	$data3y=array(starepnat($mactun,"4",$annee),starepnat($mmun ,"4",$amun),starepnat($mmdn,"4",$amdn),starepnat($mmtn,"4",$amtn),starepnat($mmqn,"4",$amqn));
	//Demandes
	$data4y=array(staque($mactun,$annee),staque($mmun,$amun),staque($mmdn,$amdn),staque($mmtn,$amtn),staque($mmqn,$amqn));
	//Inconnus
	$data5y=array(starepnat($mactun,"3",$annee),starepnat($mmun ,"3",$amun),starepnat($mmdn,"3",$amdn),starepnat($mmtn,"3",$amtn),starepnat($mmqn,"3",$amqn));
	//Connexions
	$data6y=array(status($mactun,$annee),status($mmun,$amun),status($mmdn,$amdn),status($mmtn,$amtn),status($mmqn,$amqn));
	
	// Create the graph. These two calls are always required
	$graph = new Graph(1020,510,'auto');
	$graph->SetScale("textlin");
	$graph->SetColor("#F2F2F2");
	$graph->SetY2Scale("lin",0,900);
	$graph->SetY2OrderBack(false);
	
	$graph->SetMargin(35,50,20,5);
	
	$theme_class = new UniversalTheme;
	$graph->SetTheme($theme_class);
	
	//$graph->yaxis->SetTickPositions(array(0,150,300,450,600,750,900,1050), array(100,200,300,400,500));
	//$graph->y2axis->SetTickPositions(array(30,40,50,60,70,80,90));
	$months = array($mois_actu,$mois_m_u,$mois_m_d,$mois_m_t,$mois_m_q);
	$graph->SetBox(false);
	
	$graph->ygrid->SetFill(false);
	$graph->xaxis->SetTickLabels(array('A','B','C','D'));
	$graph->yaxis->HideLine(false);
	$graph->yaxis->HideTicks(false,false);
	// Setup month as labels on the X-axis
	$graph->xaxis->SetTickLabels($months);
	
	// Create the bar plots
	// Récupérables
	$b1plot = new BarPlot($data1y);
	//Indus Probables
	$b2plot = new BarPlot($data2y);
	//Ambigus
	$b3plot = new BarPlot($data3y);
	//Demandes
	$b4plot = new BarPlot($data4y);
	$b5plot = new BarPlot($data5y);
	
	$lplot = new LinePlot($data6y);
	
	// Create the grouped bar plot
	$gbplot = new AccBarPlot(array($b1plot,$b2plot,$b3plot,$b5plot));
	$gbbplot = new GroupBarPlot(array($b4plot,$gbplot));
	
	// ...and add it to the graPH
	$graph->Add($gbbplot);
	$graph->AddY2($lplot);
	
	// Récupérables
	$b1plot->SetColor("#1D6F99");
	$b1plot->SetFillColor("#1D6F99");
	$b1plot->SetLegend("Récupérables");
	
	//Indus Probables
	$b2plot->SetColor("#FFBEB5");
	$b2plot->SetFillColor("#FFBEB5");
	$b2plot->SetLegend("Indus Probables");
	//Ambigus
	$b3plot->SetColor("#CC646C");
	$b3plot->SetFillColor("#CC646C");
	$b3plot->SetLegend("Ambigus");
	//Demandes
	$b4plot->SetColor("#d2bf64");
	$b4plot->SetFillColor("#d2bf64");
	$b4plot->SetLegend("Demandes");
	$b4plot->value->SetFormat('%d');
	$b4plot->value->Show();
	$b4plot->value->SetColor('#d2bf64');
	//Inconnus
	$b5plot->SetColor("#64A8CC");
	$b5plot->SetFillColor("#64A8CC");
	$b5plot->SetLegend("Inconnus");
	//Connexions
	$lplot->SetBarCenter();
	$lplot->SetColor("black");
	$lplot->SetLegend("Connexions");
	$lplot->mark->SetType(MARK_SQUARE,'',1.0);
	$lplot->mark->SetWeight(2);
	$lplot->mark->SetWidth(5);
	$lplot->mark->setColor("black");
	$lplot->mark->setFillColor("black");
	$lplot->value->SetFormat('%d');
	$lplot->value->Show();
	$lplot->value->SetColor('black');
	
	$graph->legend->SetFrameWeight(1);
	$graph->legend->SetColumns(6);
	$graph->legend->SetColor('#9b9b9b','black');
	
	$band = new PlotBand(VERTICAL,BAND_RDIAG,11,"max",'khaki4');
	$band->ShowFrame(true);
	$band->SetOrder(DEPTH_BACK);
	$graph->Add($band);
	
	$graph->title->Set("Progression des demandes");
	
	// Display the graph
	$graph->Stroke();
}else{
	header ('Location: index.php');
}
?>