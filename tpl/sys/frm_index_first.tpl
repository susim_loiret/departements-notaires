{# Formulaire déverrouillage du compte #}
<br />

<div class="error_msg">
  {{error_msg}}
</div>

<div id="invit_bis">
  <form action="index.php?index=first" method="post" name="modif_mdp">
    <table>
      <tr>
        <td colspan="2" height="100px"><p id="message_bis">
            Pour votre première connexion merci de personnaliser votre mot de passe et accepter les conditions
            d'utilisation.<br /> 
            Le mot de passe doit contenir 8 caractères avec au moins une majuscule, un chiffre et
            un caractère spécial (?/!*%$).
          </p></td>
      </tr>
      <tr>
        <td><label id="ident">Mot de passe</label></td>
        <td><input type="password" class="input_connexion" name="pass" required="required"></td>
      </tr>
      <tr>
        <td><label id="ident">Confirmation du mot de passe</label></td>
        <td align="right"><input type="password" class="input_connexion" name="pass_confirm" required="required"></td>
      </tr>
      <tr>
        <td width="600"><p class="invit_bis">
            En cochant cette case, vous reconnaissez avoir pris connaissance et accepter les 
            <a class="index_bis"
              href="index.php?index=cgu" class="deconnexion" target="_blank">Conditions Générales d'Utilisation</a> de cette
            application.
          </p>
        </td>
        <td align="left"><label class="checkbox"> <input type="checkbox" name="cgu_accept" id="" value="true"
            required="required"><span style="margin-right: 5px;"></span></label></td>
      </tr>
      <tr>
        <td colspan="2" height="100px" align="center"><input type="submit" id="submit_deverouille" name="inscription" value="Valider"></td>
      </tr>
    </table>
  </form>
</div>

<div class="info_msg">
  {{message}}
</div>
